# Projet Réseau Sockets M1

## Description du projet

Ce projet est un chat en réseau entre 2 machines clientes et un serveur de communication

## Client

Nous avons un client qui enverra et recevra des informations d'un serveur via des sockets

## Serveur

Le serveur répondra aux sockets du client

## Compilation
Se placer à la racine du projet et faire cette commande
```
./compileAll.sh
```

## Lancement du serveur
```
./output/server
```

## Lancement d'un client
```
./output/client
```

## Commandes utiles

git config --global user.email "maxime.potet@free.fr"
git config --global user.name "maximeptt"
