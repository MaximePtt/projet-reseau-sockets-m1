#include <arpa/inet.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

#define SERVER_ADDRESS "127.0.0.1"
#define SERVER_PORT 5000

int isFirst = 1;

void *receive_message(void *arg) {
  int sock = *(int *)arg;
  char buffer[1024] = {0};

  while (1) {
    if(!isFirst){
      for (int i = 0; i < 1024; i++)
        buffer[i] = 0;
      int valread = read(sock, buffer, sizeof(buffer));
      if (valread > 0) {
        printf("%s\0", buffer);
      } else {
        printf("Connection closed by server.\n");
        break;
      }
    }
  }

  pthread_exit(NULL);
}

int is_whitespace(char c) {
  return (c == ' ' || c == '\t' || c == '\n' || c == '\r');
}

int main(int argc, char const *argv[]) {
  struct sockaddr_in server;
  int sock = 0;
  char buffer[1024] = {0};
  pthread_t thread;

  // Create the socket
  if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
    printf("Socket creation error\n");
    return -1;
  }

  // Set the server address and port
  server.sin_family = AF_INET;
  server.sin_port = htons(SERVER_PORT);

  // Convert IPv4 and IPv6 addresses from text to binary form
  if (inet_pton(AF_INET, SERVER_ADDRESS, &server.sin_addr) <= 0) {
    printf("Invalid address or address not supported\n");
    return -1;
  }

  // Connect to the server
  if (connect(sock, (struct sockaddr *)&server, sizeof(server)) < 0) {
    printf("Connection error\n");
    return -1;
  }

  // Create the receive message thread
  if (pthread_create(&thread, NULL, receive_message, &sock) != 0) {
    printf("Thread creation error\n");
    return -1;
  }

  

  // Wait for the server response first
  for (int i = 0; i < 1024; i++)
    buffer[i] = 0;
  int valread = read(sock, buffer, sizeof(buffer));
  if (valread > 0) {
    printf("%s\0", buffer);
    isFirst = 0;
  }

// Detach the thread to run in the background
  pthread_detach(thread);

  while (1) {
    for (int i = 0; i < 1024; i++)
      buffer[i] = 0;
    // Read the message from the user
    scanf("%[^\n]%*c", buffer);
    // Remove trailing newline character
    buffer[strcspn(buffer, "\n")] = '\0';

    // Check if the input string is empty or contains only whitespace characters
    int is_empty = 1;
    for (int i = 0; i < strlen(buffer); i++) {
      if (!is_whitespace(buffer[i])) {
        is_empty = 0;
        break;
      }
    }

    // Send the message to the server if it's not empty
    if (!is_empty) {
      send(sock, buffer, strlen(buffer), 0);
    }

    // Exit loop if user entered "quit"
    if (strncmp(buffer, "quit", 4) == 0) {
      break;
    }
  }

  // Close the socket to exit
  close(sock);

  return 0;
}
