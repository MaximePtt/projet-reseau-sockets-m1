#include <arpa/inet.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

#define PORT 5000
#define MAX_CLIENTS 10
#define TAILLE_MAX_NOM 256
#define MAX_USERS_BY_CANAL 100
#define MAX_CANALS_BY_USER 5
#define NB_CANALS 100
#define MAX_USERS_REGISTERED 100

int nb_connected_clients = 0;
int client_sockets[MAX_CLIENTS] = {0}; // Tableau de sockets
char buffer[1024] = {0};

// Type d'un utilisateur
typedef struct Tuser {
  char *usernameTuser;                 // Nom de l'utilisateur
  char *passwordTuser;                 // Mot de passe de l'utilisateur
  int canalsTuser[MAX_CANALS_BY_USER]; // Liste des canaux que l'utilisateur a
                                       // rejoint (5 canaux maximum)
  int nbCanals;
  int socket;
} Tuser;

// Type d'un canal
typedef struct Tcanal {
  int numTcanal;                         // Numéro du canal
  char *usersTcanal[MAX_USERS_BY_CANAL]; // Liste des utilisateurs ayant rejoint
                                         // le canal
  int nbUsersTcanal;
} Tcanal;

Tuser AllUsers[MAX_USERS_REGISTERED];
int nbUsersRegistered = 0;
Tcanal AllCanals[NB_CANALS];

void AllCanals_update(Tcanal canal) { AllCanals[canal.numTcanal] = canal; }

void AllUsers_update(Tuser user) {
  for (int i = 0; i < nbUsersRegistered; i++) {
    printf("%s\n", AllUsers[i].usernameTuser);
    if (strcmp(AllUsers[i].usernameTuser, user.usernameTuser) == 0) {
      AllUsers[i] = user;
      break;
    }
  }
}

Tuser AllUsers_get_user(char *username) {
  Tuser userFound;

  for (int i = 0; i < nbUsersRegistered; i++) {
    printf("%s\n", "AllUsers_get_user");
    printf("%s\n", AllUsers[i].usernameTuser);
    if (strcmp(AllUsers[i].usernameTuser, username) == 0) {
      userFound = AllUsers[i];
      break;
    }
  }
  return userFound;
}

void Tuser_join_canal(Tuser user, int canalToJoin) {
  user = AllUsers_get_user(user.usernameTuser);
  if (user.nbCanals == 5) {
    printf("Erreur: L'utilisateur %s a déjà 5 canaux actifs: %d, %d, %d, %d et "
           "%d\n",
           user.usernameTuser, user.canalsTuser[0], user.canalsTuser[1],
           user.canalsTuser[2], user.canalsTuser[3], user.canalsTuser[4]);
  } else {
    user.nbCanals++;
    user.canalsTuser[user.nbCanals - 1] = canalToJoin;
    AllUsers_update(user);
  }
}

int AllUsers_user_exists(char *username) {
  for (int i = 0; i < nbUsersRegistered; i++) {
    if (AllUsers[i].usernameTuser != NULL) {
      printf("%s est inscrit\n", AllUsers[i].usernameTuser);
      if (strcmp(AllUsers[i].usernameTuser, username) == 0) {
        return 1;
      }
    }
  }
  return 0;
}

Tcanal AllCanals_get_canal(int numCanal) { return AllCanals[numCanal]; }

void Tcanal_add_user(Tcanal canal, char *username) {
  canal = AllCanals_get_canal(canal.numTcanal);
  if (canal.nbUsersTcanal == MAX_USERS_BY_CANAL) {
    printf("Erreur: Le canal %d a déjà %d personnes\n", canal.numTcanal,
           MAX_USERS_BY_CANAL);
  } else {
    canal.nbUsersTcanal++;
    canal.usersTcanal[canal.nbUsersTcanal - 1] = malloc(strlen(username) + 1);
    strcpy(canal.usersTcanal[canal.nbUsersTcanal - 1], username);
    AllCanals_update(canal);
  }
}

void join_canal(char *username, int numCanal) {
  Tuser user = AllUsers_get_user(username);
  Tcanal canal = AllCanals_get_canal(numCanal);
  Tuser_join_canal(user, numCanal);
  Tcanal_add_user(canal, username);
}

void Tuser_Create(char *usernameTuser, char *passwordTuser) {
  Tuser user;
  user.usernameTuser = malloc(strlen(usernameTuser) + 1);
  strcpy(user.usernameTuser, usernameTuser);
  user.passwordTuser = malloc(strlen(passwordTuser) + 1);
  strcpy(user.passwordTuser, passwordTuser);
  user.nbCanals = 0;
  for (int i = 0; i < MAX_CANALS_BY_USER; i++) {
    user.canalsTuser[i] = -1; // -1 signifie pas de canal
  }
  if (nbUsersRegistered >= MAX_USERS_REGISTERED) {
    printf("Erreur: Le nombre maximum d'utilisateurs est atteint (%d)",
           MAX_USERS_REGISTERED);
  } else {
    AllUsers[nbUsersRegistered++] =
        user; // stocke le nouvel utilisateur dans le tableau AllUsers
    join_canal(usernameTuser, 0); // Canal 0 général par défaut
  }
}

void Tuser_leave_canal(Tuser user, int canalToLeave) {
  if (user.nbCanals == 0)
    printf("Erreur: L'utilisateur %s n'a pas de canaux à quitter\n",
           user.usernameTuser);
  else {
    int indiceCanalToLeave = -1;
    for (int i = 0; i < user.nbCanals; i++) {
      if (user.canalsTuser[i] == canalToLeave) {
        indiceCanalToLeave = i;
        break;
      }
    }
    if (indiceCanalToLeave == -1)
      printf("Erreur: L'utilisateur %s n'est pas sur le canal %d\n",
             user.usernameTuser, canalToLeave);
    else {
      for (int i = indiceCanalToLeave; i < user.nbCanals - 1; i++) {
        user.canalsTuser[i] = user.canalsTuser[i + 1];
      }
      user.nbCanals--;
      user.canalsTuser[user.nbCanals] = -1;
      AllUsers_update(user);
    }
  }
}

void Tcanal_Create(int num) {
  Tcanal canal;
  canal.nbUsersTcanal = 0;
  canal.numTcanal = num;
  for (int i = 1; i < MAX_CANALS_BY_USER; i++) {
    canal.usersTcanal[i] = ""; // chaine vide signifie pas de joueur
  }
  AllCanals[num] = canal; // L'indice du tableau de canaux correspond au numéro
                          // du canal contenu
}

void Tcanal_eject_user(Tcanal canal, char *userToEject) {
  int indiceUserToEject = -1;
  for (int i = 0; i < canal.nbUsersTcanal; i++) {
    if (canal.usersTcanal[i] == userToEject) {
      indiceUserToEject = i;
      break;
    }
  }
  if (indiceUserToEject == -1)
    printf("Erreur: Le canal %d ne contient pas l'utilisateur %s\n",
           canal.numTcanal, userToEject);
  else {
    for (int i = indiceUserToEject; i < canal.nbUsersTcanal - 1; i++) {
      canal.usersTcanal[i] = canal.usersTcanal[i + 1];
    }
    canal.nbUsersTcanal--;
    canal.usersTcanal[canal.nbUsersTcanal] = "";
    AllCanals_update(canal);
  }
}

void leave_canal(char *username, int numCanal) {
  Tuser user = AllUsers_get_user(username);
  Tcanal canal = AllCanals_get_canal(numCanal);
  Tuser_leave_canal(user, numCanal);
  Tcanal_eject_user(canal, username);
}

void send_msg_to_canal(int numCanal, int socket) {
  // Envoyer le message à tous les clients connectés au canal actuel
  for (int i = 0; i < AllCanals[numCanal].nbUsersTcanal; i++) {
    Tuser receiverUser = AllUsers_get_user(AllCanals[numCanal].usersTcanal[i]);
    send(receiverUser.socket, buffer, strlen(buffer), 0);
  }
}

// Define a function that will handle each client connection
void *handle_client(void *arg) {
  int valread;
  int client_socket = *((int *)arg);
  int canal = 0; // Le canal actuel du client (0 = canal général)
  free(arg);

  if (nb_connected_clients == MAX_CLIENTS) {
    perror("Already max clients connected");
  } else {

    int isAskingUsername = 1;
    int waitingForNewPwd = 0;
    int waitingForLoginPwd = 0;
    nbUsersRegistered = 0;
    char *username = NULL;
    Tuser user;
    int client_number = -1;
    for (int i = 0; i < MAX_CLIENTS; i++) {
      if (client_sockets[i] == 0) {
        client_number = i;
        break;
      }
    }

    nb_connected_clients++;

    client_sockets[client_number] = client_socket;

    printf("Client socket : %d\n", client_socket);
    printf("New client connected\n");
    char *ask_username_msg =
        "[INFO] Bonjour, veuillez indiquer votre pseudo :\n";
    strcpy(buffer, ask_username_msg);
    send(client_socket, buffer, strlen(buffer), 0);
    // Read and write data to the client
    while (1) {
      // envoyer un message de bienvenue au client
      valread = read(client_socket, buffer, sizeof(buffer));
      if (valread == 0) {
        nb_connected_clients--;
        client_sockets[client_number] = 0;
        printf("Client disconnected\n");
        close(client_socket);
        return NULL;
      } else if (valread < 0) {
        perror("Read failed");
        close(client_socket);
        return NULL;
      } else {
        if (isAskingUsername) {
          username = buffer;
          if (!AllUsers_user_exists(username)) {
            char *first_conn_pwd_msg =
                "[INFO] Ceci est votre première connection !\n"
                "[INFO] Veuillez renseigner un mot de passe:\n";
            strcpy(buffer, first_conn_pwd_msg);
            send(client_socket, buffer, strlen(buffer), 0);
            waitingForNewPwd =
                1; // Si Le user n'existe pas, on attend le nouveau mdp
          } else {
            user = AllUsers_get_user(username);
            char *conn_pwd_msg =
                "[INFO] Veuillez renseigner votre mot de passe:\n";
            strcpy(buffer, conn_pwd_msg);
            send(client_socket, buffer, strlen(buffer), 0);
            waitingForLoginPwd =
                1; // Si le user existe, on attend le mdp correspondant
          }
          isAskingUsername = 0;
        } else if (waitingForNewPwd) {
          char *password = buffer;
          Tuser_Create(username, password);
          user = AllUsers_get_user(username);
          printf("L'user '%s' s'est inscrit\n", user.usernameTuser);
          user.socket = client_socket;
          char *user_created_msg =
              "[INFO] Votre utilisateur a bien été créé !\n"
              "[INFO] Vous êtes donc connecté !\n";
          strcpy(buffer, user_created_msg);
          send(client_socket, buffer, strlen(buffer), 0);
          waitingForNewPwd =
              0; // On ne re-demande pas la connection après création
          waitingForLoginPwd =
              0; // On ne re-demande pas la connection après création
        } else if (waitingForLoginPwd) {
          user = AllUsers_get_user(username);
          user.socket = client_socket;
          if (strcmp(user.passwordTuser, buffer) == 0) {
            char *login_ok_msg =
                "[INFO] Mot de passe correct, vous êtes bien connecté !\n";
            strcpy(buffer, login_ok_msg);
            send(client_socket, buffer, strlen(buffer), 0);
            waitingForLoginPwd = 0;
          } else {
            char *login_failed_msg =
                "[INFO] Mot de passe incorrect, veuillez réessayer :\n";
            strcpy(buffer, login_failed_msg);
            send(client_socket, buffer, strlen(buffer), 0);
          }
        } else {
          // Vérifier si le message commence par "!" pour envoyer sur un canal
          // spécifique
          if (buffer[0] == '!') {
            // Extraire le numéro de canal
            int num_channel = atoi(&buffer[1]);
            if (num_channel >= 0 && num_channel < NB_CANALS) {
              canal = num_channel;
              send_msg_to_canal(canal, client_socket);
            } else {
              char *error_num_canal =
                  "[INFO] Un canal doit être entre 0 et 100";
              strcpy(buffer, error_num_canal);
              send(client_socket, buffer, strlen(buffer), 0);
            }
            printf("Le numéro de channel est '%d'\n", num_channel);
          } else {
            canal = 0;
            printf("Canal général !\n");
            send_msg_to_canal(canal, client_socket);
          }
        }
        memset(buffer, 0, sizeof(buffer));
      }
    }
  }

  return NULL;
}

int main(int argc, char const *argv[]) {
  int server_fd, new_socket;
  struct sockaddr_in address;
  int addrlen = sizeof(address);

  // Créer les 100 canaux
  for (int i = 0; i <= 100; i++) {
    Tcanal_Create(i);
  }

  // Create the server socket
  if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
    perror("Socket creation error");
    return -1;
  }

  // Set the server address and port
  address.sin_family = AF_INET;
  address.sin_addr.s_addr = INADDR_ANY;
  address.sin_port = htons(PORT);

  // Bind the socket to the server address and port
  if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0) {
    perror("Bind failed");
    return -1;
  }

  // Start listening for incoming connections
  if (listen(server_fd, 5) < 0) {
    perror("Listen failed");
    return -1;
  }

  printf("Server listening on port %d...\n", PORT);

  // Accept incoming connections
  while (1) {
    if ((new_socket = accept(server_fd, (struct sockaddr *)&address,
                             (socklen_t *)&addrlen)) < 0) {
      perror("Accept failed");
      return -1;
    }
    // Handle each new client connection in a separate thread
    pthread_t tid;
    int *arg = malloc(sizeof(*arg));
    *arg = new_socket;
    if (pthread_create(&tid, NULL, handle_client, arg) != 0) {
      perror("Thread creation failed");
      return -1;
    }
    pthread_detach(tid);
  }

  return 0;
}
