#include <arpa/inet.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

#define PORT 5000
#define MAX_CLIENTS 10
#define TAILLE_MAX_NOM 256
#define MAX_USERS_BY_CANAL 100
#define MAX_CANALS_BY_USER 5
#define NB_CANALS 100
#define MAX_USERS_REGISTERED 100

int nb_connected_clients = 0;
int nbUsersRegistered = 0;
int client_sockets[MAX_CLIENTS] = {0}; // Tableau de sockets
char buffer[1024] = {0};

// Type d'un utilisateur
typedef struct Tuser {
  char *usernameTuser;                 // Nom de l'utilisateur
  char *passwordTuser;                 // Mot de passe de l'utilisateur
  int canalsTuser[MAX_CANALS_BY_USER]; // Liste des canaux que l'utilisateur a
                                       // rejoint (5 canaux maximum)
  int nbCanals;
  int socket;
} Tuser;

// Type d'un canal
typedef struct Tcanal {
  int numTcanal;                         // Numéro du canal
  char *usersTcanal[MAX_USERS_BY_CANAL]; // Liste des utilisateurs ayant rejoint
                                         // le canal
  int nbUsersTcanal;
} Tcanal;

Tuser AllUsers[MAX_USERS_REGISTERED];
Tcanal AllCanals[NB_CANALS];

void AllCanals_update(Tcanal canal) { AllCanals[canal.numTcanal] = canal; }

void AllUsers_update(Tuser user) {
  for (int i = 0; i < nbUsersRegistered; i++) {
    if (strcmp(AllUsers[i].usernameTuser, user.usernameTuser) == 0) {
      AllUsers[i] = user;
      break;
    }
  }
}

Tuser AllUsers_get_user(char *username) {
  Tuser userFound;

  for (int i = 0; i < nbUsersRegistered; i++) {
    if (strcmp(AllUsers[i].usernameTuser, username) == 0) {
      userFound = AllUsers[i];
      break;
    }
  }
  return userFound;
}

int Tuser_join_canal(Tuser user, int canalToJoin) {
  user = AllUsers_get_user(user.usernameTuser);
  if (user.nbCanals == 5) {
    char *full_canal_msg = malloc(
        sizeof(char) * 100); // allouer de l'espace pour la chaîne formatée
    printf("\033[31m[WARN]\033[00m L'utilisateur %s a déjà 5 canaux actifs: "
           "%d, %d, %d, %d et "
           "%d\n",
           user.usernameTuser, user.canalsTuser[0], user.canalsTuser[1],
           user.canalsTuser[2], user.canalsTuser[3], user.canalsTuser[4]);
    sprintf(full_canal_msg,
            "\033[31m[WARN]\033[00m Vous ne pouvez pas rejoindre un canal "
            "supplémentaire.\n");
    send(user.socket, full_canal_msg, strlen(full_canal_msg), 0);
    sprintf(full_canal_msg,
            "\033[32m[INFO]\033[00m Vous avez déjà 5 canaux actifs: %d, %d, "
            "%d, %d et "
            "%d.\n",
            user.canalsTuser[0], user.canalsTuser[1], user.canalsTuser[2],
            user.canalsTuser[3], user.canalsTuser[4]);
    send(user.socket, full_canal_msg, strlen(full_canal_msg), 0);
    sprintf(full_canal_msg,
            "\033[34m[TIPS]\033[00m Faire &quitter <CANAL> pour quitter un "
            "canal.\n");
    send(user.socket, full_canal_msg, strlen(full_canal_msg), 0);
  } else {
    int already_joined = 0;
    for (int i = 0; i < user.nbCanals; i++) {
      if (user.canalsTuser[i] == canalToJoin) {
        already_joined = 1;
        break;
      }
    }
    if (already_joined) {
      char error_message[] =
          "\033[31m[WARN]\033[00m Vous êtes déjà sur ce canal.\n";
      send(user.socket, error_message, strlen(error_message), 0);
    } else {
      user.nbCanals++;
      user.canalsTuser[user.nbCanals - 1] = canalToJoin;
      AllUsers_update(user);
      printf("\033[32m[INFO]\033[00m %s a rejoint le canal %d\n",
             user.usernameTuser, canalToJoin);
      char *joined_chanel_msg = malloc(
          sizeof(char) * 100); // allouer de l'espace pour la chaîne formatée
      sprintf(joined_chanel_msg,
              "\033[32m[INFO]\033[00m Vous avez rejoint le canal %d\n",
              canalToJoin);
      send(user.socket, joined_chanel_msg, strlen(joined_chanel_msg), 0);
      return 1;
    }
  }
  return 0;
}

int AllUsers_user_exists(char *username) {
  for (int i = 0; i < nbUsersRegistered; i++) {
    if (strcmp(AllUsers[i].usernameTuser, username) == 0) {
      return 1;
    }
  }
  return 0;
}

Tcanal AllCanals_get_canal(int numCanal) { return AllCanals[numCanal]; }

void Tcanal_add_user(Tcanal canal, char *username) {
  canal = AllCanals_get_canal(canal.numTcanal);
  if (canal.nbUsersTcanal == MAX_USERS_BY_CANAL) {
    printf("\033[31m[WARN]\033[00m Le canal %d a déjà %d personnes\n",
           canal.numTcanal, MAX_USERS_BY_CANAL);
  } else {
    canal.nbUsersTcanal++;
    canal.usersTcanal[canal.nbUsersTcanal - 1] = malloc(strlen(username) + 1);
    strcpy(canal.usersTcanal[canal.nbUsersTcanal - 1], username);
    AllCanals_update(canal);
  }
}

void join_canal(char *username, int numCanal) {
  Tuser user = AllUsers_get_user(username);
  Tcanal canal = AllCanals_get_canal(numCanal);
  if (Tuser_join_canal(user, numCanal)) {
    Tcanal_add_user(canal, username);
  }
}

void Tuser_Create(char *usernameTuser, char *passwordTuser) {
  Tuser user;
  user.usernameTuser = malloc(strlen(usernameTuser) + 1);
  strcpy(user.usernameTuser, usernameTuser);
  user.passwordTuser = malloc(strlen(passwordTuser) + 1);
  strcpy(user.passwordTuser, passwordTuser);
  user.nbCanals = 0;
  for (int i = 0; i < MAX_CANALS_BY_USER; i++) {
    user.canalsTuser[i] = -1; // -1 signifie pas de canal
  }
  if (nbUsersRegistered >= MAX_USERS_REGISTERED) {
    printf("\033[31m[WARN]\033[00m Le nombre maximum d'utilisateurs est "
           "atteint (%d)\n",
           MAX_USERS_REGISTERED);
  } else {
    AllUsers[nbUsersRegistered] =
        user; // stocke le nouvel utilisateur dans le tableau AllUsers
    nbUsersRegistered++;
    join_canal(usernameTuser, 0); // Canal 0 général par défaut
  }
}

void Tuser_leave_canal(Tuser user, int canalToLeave) {
  if (user.nbCanals == 0)
    printf(
        "\033[31m[WARN]\033[00m L'utilisateur %s n'a pas de canaux à quitter\n",
        user.usernameTuser);
  else {
    int indiceCanalToLeave = -1;
    for (int i = 0; i < user.nbCanals; i++) {
      if (user.canalsTuser[i] == canalToLeave) {
        indiceCanalToLeave = i;
        break;
      }
    }
    if (indiceCanalToLeave == -1) {
      printf(
          "\033[31m[WARN]\033[00m L'utilisateur %s n'est pas sur le canal %d\n",
          user.usernameTuser, canalToLeave);
      char *user_no_on_canal = malloc(
          sizeof(char) * 100); // allouer de l'espace pour la chaîne formatée
      sprintf(user_no_on_canal,
              "\033[32m[INFO]\033[00m Vous n'êtes pas sur le canal %d.\n",
              canalToLeave);
      send(user.socket, user_no_on_canal, strlen(user_no_on_canal), 0);
      sprintf(user_no_on_canal, "\033[34m[TIPS]\033[00m Faites &jc <CANAL> "
                                "pour rejoindre le canal souhaité.\n");
      send(user.socket, user_no_on_canal, strlen(user_no_on_canal), 0);
    } else {
      for (int i = indiceCanalToLeave; i < user.nbCanals - 1; i++) {
        user.canalsTuser[i] = user.canalsTuser[i + 1];
      }
      user.nbCanals--;
      user.canalsTuser[user.nbCanals] = -1;
      AllUsers_update(user);
      printf("\033[32m[INFO]\033[00m %s a quitté le canal %d.\n",
             user.usernameTuser, canalToLeave);
      char *quitted_chanel_msg = malloc(
          sizeof(char) * 100); // allouer de l'espace pour la chaîne formatée
      sprintf(quitted_chanel_msg,
              "\033[32m[INFO]\033[00m Vous avez quitté le canal %d.\n",
              canalToLeave);
      send(user.socket, quitted_chanel_msg, strlen(quitted_chanel_msg), 0);
    }
  }
}

void Tcanal_Create(int num) {
  Tcanal canal;
  canal.nbUsersTcanal = 0;
  canal.numTcanal = num;
  for (int i = 1; i < MAX_CANALS_BY_USER; i++) {
    canal.usersTcanal[i] = ""; // chaine vide signifie pas de joueur
  }
  AllCanals[num] = canal; // L'indice du tableau de canaux correspond au numéro
                          // du canal contenu
}

void Tcanal_eject_user(Tcanal canal, char *userToEject) {
  int indiceUserToEject = -1;
  for (int i = 0; i < canal.nbUsersTcanal; i++) {
    if (strcmp(userToEject, canal.usersTcanal[i]) == 0) {
      indiceUserToEject = i;
      break;
    }
  }
  if (indiceUserToEject == -1)
    printf("Erreur: Le canal %d ne contient pas l'utilisateur %s\n",
           canal.numTcanal, userToEject);
  else {
    for (int i = indiceUserToEject; i < canal.nbUsersTcanal - 1; i++) {
      canal.usersTcanal[i] = canal.usersTcanal[i + 1];
    }
    canal.nbUsersTcanal--;
    canal.usersTcanal[canal.nbUsersTcanal] = "";
    AllCanals_update(canal);
    printf("%s EJECTED\n", userToEject);
  }
}

void leave_canal(char *username, int numCanal) {
  Tuser user = AllUsers_get_user(username);
  Tcanal canal = AllCanals_get_canal(numCanal);
  Tuser_leave_canal(user, numCanal);
  Tcanal_eject_user(canal, username);
}

char *format_sentence(char *sentence) {
  size_t len = strlen(sentence);
  // Alloue une nouvelle zone de mémoire pour la chaîne modifiée
  char *formatted = malloc(len + 2); // +2 pour le point et le caractère nul
  // Copie la chaîne d'origine dans la nouvelle zone de mémoire
  strncpy(formatted, sentence, len);
  // Met la première lettre en majuscule
  if (formatted[0] >= 'a' && formatted[0] <= 'z') {
    formatted[0] = formatted[0] - 'a' + 'A';
  }
  // Ajoute un point à la fin si la phrase n'en a pas déjà un
  if (len > 0 && formatted[len - 1] != '.') {
    formatted[len] = '.';
    formatted[len + 1] = '\0';
  }

  return formatted;
}

void send_msg_to_canal(int numCanal, Tuser user) {
  user = AllUsers_get_user(user.usernameTuser);
  Tcanal canal = AllCanals_get_canal(numCanal);
  int joined = 0;

  // Vérifier si l'utilisateur a rejoint le canal
  for (int i = 0; i < user.nbCanals; i++) {
    if (user.canalsTuser[i] == numCanal) {
      joined = 1;
      break;
    }
  }
  if (joined) {
    char receiverUsername[TAILLE_MAX_NOM];
    char *msg = (char *)malloc((strlen(buffer) + 1) *
                               sizeof(char)); // +1 pour le caractère nul
    strcpy(msg, buffer);

    int tmp;
    sscanf(msg, "!%d %[^\n]", &tmp, msg);

    msg = format_sentence(msg);

    char *str = (char *)malloc(100 * sizeof(char));
    sprintf(str, "\033[36m[@%d]\033[00m %s : %s\n", numCanal,
            user.usernameTuser, msg);
    printf("%s", str); // Le serveur voit tout ce qu'il se passe sur les canaux

    // Envoyer le message à tous les clients connectés au canal actuel
    for (int i = 0; i < AllCanals[numCanal].nbUsersTcanal; i++) {
      Tuser receiverUser =
          AllUsers_get_user(AllCanals[numCanal].usersTcanal[i]);
      send(receiverUser.socket, str, strlen(str), 0);
    }
    free(msg); // libérer l'espace alloué pour la variable "msg" lorsque vous
               // avez terminé de l'utiliser
    free(str); // libérer l'espace alloué pour la variable "str" lorsque vous
               // avez terminé de l'utiliser

  } else {
    printf("\033[32m[INFO]\033[00m %s a tenté d'écrire sur le canal %d.\n",
           user.usernameTuser, numCanal);
    char *no_user_in_canal_msg = malloc(
        sizeof(char) * 100); // allouer de l'espace pour la chaîne formatée
    sprintf(no_user_in_canal_msg,
            "\033[31m[WARN]\033[00m Vous n'êtes pas sur le canal %d.\n",
            numCanal);
    send(user.socket, no_user_in_canal_msg, strlen(no_user_in_canal_msg), 0);
    sprintf(no_user_in_canal_msg,
            "\033[34m[TIPS]\033[00m Faites '&jc <CANAL>' pour le rejoindre.\n");
    send(user.socket, no_user_in_canal_msg, strlen(no_user_in_canal_msg), 0);
    free(no_user_in_canal_msg); // libérer l'espace alloué pour la variable
                                // "msg" lorsque vous avez terminé de l'utiliser
  }
}

void send_private_msg(char *senderUsername, char *receiverUsername,
                      char *message) {
  Tuser sender = AllUsers_get_user(senderUsername);
  Tuser receiver = AllUsers_get_user(receiverUsername);

  if (message == NULL || strlen(message) == 0) {
    char *empty_message_msg = "\033[31m[WARN]\033[00m Le message est vide.\n";
    send(sender.socket, empty_message_msg, strlen(empty_message_msg), 0);
    printf("\033[31m[WARN]\033[00m L'utilisateur %s a tenté d'envoyer un MP à "
           "%s sans "
           "message.\n",
           senderUsername, receiverUsername);
    return;
  }

  if (strcmp(senderUsername, receiverUsername) == 0) {
    char *self_message_msg =
        "\033[31m[WARN]\033[00m Vous ne pouvez pas vous envoyer un "
        "message privé à vous-même.\n";
    send(sender.socket, self_message_msg, strlen(self_message_msg), 0);
    printf("\033[31m[WARN]\033[00m L'utilisateur %s a tenté de s'envoyer un "
           "message "
           "privé à "
           "lui-même.\n",
           senderUsername);
    return;
  }

  int sender_socket = sender.socket;
  int receiver_socket = receiver.socket;

  if (receiver_socket == 0 ||
      strcmp(receiver.usernameTuser, receiverUsername) != 0) {
    char *user_not_connected_msg = malloc(
        sizeof(char) * 100); // allouer de l'espace pour la chaîne formatée
    sprintf(user_not_connected_msg,
            "\033[31m[WARN]\033[00m L'utilisateur %s n'est pas connecté ou "
            "n'existe "
            "pas.\n",
            receiverUsername);
    send(sender_socket, user_not_connected_msg, strlen(user_not_connected_msg),
         0);
    free(user_not_connected_msg); // libérer l'espace alloué pour la variable
                                  // "msg" lorsque vous avez
    printf("\033[31m[WARN]\033[00m L'utilisateur %s a tenté d'envoyer un MP à "
           "%s, qui "
           "n'est "
           "pas connecté.\n",
           senderUsername, receiverUsername);
    return;
  }

  char *str = malloc(strlen(sender.usernameTuser) + strlen(message) + 10);
  sprintf(str, "\033[34m[MP de %s]\033[00m %s\n", sender.usernameTuser,
          message);
  send(receiver_socket, str, strlen(str), 0);
  free(str);

  str = malloc(strlen(sender.usernameTuser) + strlen(message) + 9);
  sprintf(str, "\033[34m[MP à %s]\033[00m %s\n", receiver.usernameTuser,
          message);
  send(sender_socket, str, strlen(str), 0);
  free(str);

  printf("\033[32m[INFO]\033[00m Message envoyé de %s à %s : %s\n",
         senderUsername, receiverUsername, message);
}

void send_help(int socket) {
  char *help_msg =
      "\033[35m[AIDE]\033[00m &aide : Afficher la liste des commandes "
      "possibles\n"
      "\033[35m[AIDE]\033[00m &jc <NUM> : Joindre le canal numéro <NUM> (De 0 "
      "à 100)\n"
      "\033[35m[AIDE]\033[00m &quitter <NUM> : Quitter le canal numéro <NUM>\n"
      "\033[35m[AIDE]\033[00m /<PSEUDO> <MESSAGE> : Envoyer un message privé à "
      "un utilisateur\n"
      "\033[35m[AIDE]\033[00m !<NUMÉRO DE CANAL> <MESSAGE> : Envoyer un "
      "message sur un canal\n"
      "\033[35m[AIDE]\033[00m <MESSAGE> : Envoyer un message sur le canal "
      "général (0 par "
      "défaut)\n";
  send(socket, help_msg, strlen(help_msg), 0);
}

// Define a function that will handle each client connection
void *handle_client(void *arg) {
  int valread;
  int client_socket = *((int *)arg);
  int canal = 0; // Le canal actuel du client (0 = canal général)
  free(arg);

  if (nb_connected_clients == MAX_CLIENTS) {
    perror("Already max clients connected");
  } else {

    int isAskingUsername = 1;
    int waitingForNewPwd = 0;
    int waitingForLoginPwd = 0;
    char *username = NULL;
    Tuser user;
    int client_number = -1;
    for (int i = 0; i < MAX_CLIENTS; i++) {
      if (client_sockets[i] == 0) {
        client_number = i;
        break;
      }
    }

    nb_connected_clients++;

    client_sockets[client_number] = client_socket;

    printf("\033[32m[INFO]\033[00m Nouvelle connexion\n");
    char *ask_username_msg =
        "\033[33m[ASK]\033[00m Bonjour, veuillez indiquer votre pseudo :\n";
    send(client_socket, ask_username_msg, strlen(ask_username_msg), 0);
    // Read and write data to the client
    while (1) {
      // envoyer un message de bienvenue au client
      valread = read(client_socket, buffer, sizeof(buffer));
      if (valread == 0) {
        nb_connected_clients--;
        client_sockets[client_number] = 0;
        printf("\033[32m[INFO]\033[00m Déconnexion d'un utilisateur\n");
        close(client_socket);
        return NULL;
      } else if (valread < 0) {
        perror("Read failed");
        close(client_socket);
        return NULL;
      } else {
        if (isAskingUsername) {
          username = malloc(sizeof(buffer));
          strcpy(username, buffer);
          if (!AllUsers_user_exists(username)) {
            char *first_conn_pwd_msg =
                "\033[32m[INFO]\033[00m Ceci est votre première connection.\n"
                "\033[33m[ASK]\033[00m Veuillez renseigner un mot de passe "
                "pour vous inscrire "
                ":\n";
            send(client_socket, first_conn_pwd_msg, strlen(first_conn_pwd_msg),
                 0);
            waitingForNewPwd =
                1; // Si Le user n'existe pas, on attend le nouveau mdp
          } else {
            user = AllUsers_get_user(username);
            char *conn_pwd_msg =
                "\033[32m[INFO]\033[00m Veuillez renseigner votre mot de "
                "passe pour vous connecter :\n";
            send(client_socket, conn_pwd_msg, strlen(conn_pwd_msg), 0);
            waitingForLoginPwd =
                1; // Si le user existe, on attend le mdp correspondant
          }
          isAskingUsername = 0;
        } else if (waitingForNewPwd) {
          char *password = buffer;
          Tuser_Create(username, password);
          user = AllUsers_get_user(username);
          printf("\033[32m[INFO]\033[00m L'utilisateur %s s'est inscrit.\n",
                 user.usernameTuser);
          user.socket = client_socket;
          AllUsers_update(user);
          char *user_created_msg =
              "\033[32m[INFO]\033[00m Votre utilisateur a bien été créé.\n"
              "\033[32m[INFO]\033[00m Vous êtes désormais connecté, vous "
              "pouvez envoyer des "
              "messages.\n"
              "\033[34m[TIPS]\033[00m Faites &aide pour davantage "
              "d'informations.\n";
          send(client_socket, user_created_msg, strlen(user_created_msg), 0);
          waitingForNewPwd =
              0; // On ne re-demande pas la connection après création
          waitingForLoginPwd =
              0; // On ne re-demande pas la connection après création
        } else if (waitingForLoginPwd) {
          user = AllUsers_get_user(username);
          user.socket = client_socket;
          AllUsers_update(user);
          if (strcmp(user.passwordTuser, buffer) == 0) {
            char *login_ok_msg =
                "\033[32m[INFO]\033[00m Votre connexion s'est effectuée avec "
                "succès !\n"
                "\033[32m[INFO]\033[00m Vous pouvez désormais envoyer des "
                "messages.\n"
                "\033[34m[TIPS]\033[00m Faire &aide pour plus d'informations\n";
            send(client_socket, login_ok_msg, strlen(login_ok_msg), 0);
            waitingForLoginPwd = 0;
          } else {
            char *login_failed_msg = "\033[32m[INFO]\033[00m Mot de passe "
                                     "incorrect, veuillez réessayer :\n";
            send(client_socket, login_failed_msg, strlen(login_failed_msg), 0);
          }
        } else if (strncmp(buffer, "&aide", 5) == 0) {
          send_help(client_socket);
        } else if (strncmp(buffer, "&jc", 3) == 0) {
          int numCanal;
          sscanf(buffer, "&jc %d", &numCanal);
          if(numCanal >= NB_CANALS || numCanal < 0) {
            char error_message[] =
                "\033[31m[WARN]\033[00m Un canal doit être entre 0 et 99.\n";
            send(user.socket, error_message, strlen(error_message), 0);
          } else{
            join_canal(user.usernameTuser, numCanal);
          }
        } else if (strncmp(buffer, "&quitter", 8) == 0) {
          int numCanal;
          sscanf(buffer, "&quitter %d", &numCanal);
          leave_canal(user.usernameTuser, numCanal);

        } else if (buffer[0] == '/') {
          char receiverUsername[TAILLE_MAX_NOM];
          char *msg = (char *)malloc(100 * sizeof(char));
          sscanf(buffer, "/%s %[^\n]", receiverUsername, msg);
          send_private_msg(user.usernameTuser, receiverUsername, msg);
        } else if (buffer[0] == '!') {
          // Extraire le numéro de canal
          int num_channel = atoi(&buffer[1]);
          if (num_channel >= 0 && num_channel < NB_CANALS) {
            canal = num_channel;
            send_msg_to_canal(canal, user);
          } else {
            char *error_num_canal =
                "\033[31m[WARN]\033[00m Un canal doit être entre 0 et 99.\n";
            send(client_socket, error_num_canal, strlen(error_num_canal), 0);
          }
        } else {
          canal = 0;
          send_msg_to_canal(canal, user);
        }
        memset(buffer, 0, sizeof(buffer));
        for (int i = 0; i < 1024; i++)
          buffer[i] = 0;
      }
    }
  }

  return NULL;
}

int main(int argc, char const *argv[]) {
  int server_fd, new_socket;
  struct sockaddr_in address;
  int addrlen = sizeof(address);

  // Créer les 100 canaux
  for (int i = 0; i <= 100; i++) {
    Tcanal_Create(i);
  }

  // Create the server socket
  if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
    perror("Socket creation error");
    return -1;
  }

  // Set the server address and port
  address.sin_family = AF_INET;
  address.sin_addr.s_addr = INADDR_ANY;
  address.sin_port = htons(PORT);

  // Bind the socket to the server address and port
  if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0) {
    perror("Bind failed");
    return -1;
  }

  // Start listening for incoming connections
  if (listen(server_fd, 5) < 0) {
    perror("Listen failed");
    return -1;
  }

  printf("\033[32m[INFO]\033[00m Server lancé sur le port %d\n", PORT);
  printf("\033[32m[INFO]\033[00m En attente de connexions entrantes ...\n");
  // Accept incoming connections
  while (1) {
    if ((new_socket = accept(server_fd, (struct sockaddr *)&address,
                             (socklen_t *)&addrlen)) < 0) {
      perror("Accept failed");
      return -1;
    }
    // Handle each new client connection in a separate thread
    pthread_t tid;
    int *arg = malloc(sizeof(*arg));
    *arg = new_socket;
    if (pthread_create(&tid, NULL, handle_client, arg) != 0) {
      perror("Thread creation failed");
      return -1;
    }
    pthread_detach(tid);
  }

  return 0;
}
